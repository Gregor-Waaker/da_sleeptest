requirejs.config({
    paths: {
        "jquery": "Scripts/jquery-2.1.4",
        "hammerjs": "Scripts/hammer"
    }
});
require(["app"], (main) => {
    var app = main.Screens.getInstance();
    app.showStartScreen();
});
