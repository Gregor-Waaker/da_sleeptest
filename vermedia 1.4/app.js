define(["require", "exports", "jquery", "hammerjs"], function (require, exports, $, hammerjs) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Screens = exports.Product = exports.Advies = exports.AdviesCaroussel = exports.Test = exports.Start = exports.BaseScreen = void 0;
    class BaseScreen {
        constructor(screenName) {
            this.screenName = screenName;
        }
        showScreen(oldscreen) {
            this.fadeOut(oldscreen);
            console.log("start")
            function preloadImages(array) {
                if (!preloadImages.list) {
                    preloadImages.list = [];
                }
                var list = preloadImages.list;
                for (var i = 0; i < array.length; i++) {
                    var img = new Image();
                    img.onload = function() {
                        var index = list.indexOf(this);
                        if (index !== -1) {
                            list.splice(index, 1);
                        }
                    }
                    list.push(img);
                    img.src = array[i];
                }
            }
            
            preloadImages(["Content/Products/DA_adviespagina.jpg", "Content/Products/valdispert-450mg-40x_3.jpg", 
            "Content/Products/valdispert-stress-moments-extrasterk-20tabs_4.png", "Content/Products/valdispert-rust_3.jpg", 
            "Content/Products/valdispert-450mg-40x_3.jpg"]);
        }
        getScreenName() {
            return this.screenName;
        }
        fadeOut(oldscreen) {
            var contentDiv = $('#content:visible');
            if (contentDiv[0].children.length != 0) {
                contentDiv.animate({
                    opacity: '0'
                }, 'fast', () => {
                    if (oldscreen != "") {
                        var screenDiv = $('#' + oldscreen);
                        $('#content').children().appendTo(screenDiv);
                    }
                    this.fadeIn();
                });
            }
            else {
                this.fadeIn();
            }
        }
        fadeIn() {
            var contentDiv = $('#content');
            contentDiv.empty();
            $('#' + this.screenName).children().appendTo(contentDiv);
            contentDiv.css('opacity', "0");
            contentDiv.animate({
                opacity: '1'
            }, 'fast');
        }
    }
    exports.BaseScreen = BaseScreen;
    class Start extends BaseScreen {
        constructor() {
            super('start');
            $('#btnStartTest').click(this.startTest);
        }
        startTest() {
            Screens.getInstance().showTestScreen();
        }
    }
    exports.Start = Start;
    class HashMap {
        constructor() {
            this.length = 0;
            this.values = [];
            this.index = {};
            this.keys = [];
        }
        add(key, value) {
            if (key === undefined && value === undefined)
                return undefined;
            var previous = undefined;
            if (this.hasKey(key)) {
                previous = this.values[this.index[key].data];
                this.values[this.index[key].data] = undefined;
                this.keys[this.index[key].key] = undefined;
            }
            else {
                this.length++;
            }
            this.index[key] = {
                data: this.values.push(value) - 1,
                key: this.keys.push(key) - 1
            };
            return previous;
        }
        get(key) {
            if (this.hasKey(key))
                return this.values[this.index[key].data];
        }
        hasKey(key) {
            return (this.index[key] !== undefined);
        }
        remove(key) {
            if (this.hasKey(key)) {
                var previous = this.values[this.index[key].data];
                this.values[this.index[key].data] = undefined;
                this.keys[this.index[key].key] = undefined;
                this.length--;
                delete this.index[key];
                return previous;
            }
            else {
                return undefined;
            }
        }
        each(fn) {
            if (typeof fn != 'function')
                return;
            for (var i = 0; i < this.values.length; i++) {
                fn(this.values[i]);
            }
        }
        clear() {
            this.values.length = 0;
            this.index = {};
            this.keys.length = 0;
            this.length = 0;
        }
    }
    var Categorie;
    (function (Categorie) {
        Categorie[Categorie["ALGEMEEN"] = 0] = "ALGEMEEN";
        Categorie[Categorie["SLAPEN"] = 1] = "SLAPEN";
        Categorie[Categorie["SPANNING"] = 2] = "SPANNING";
        Categorie[Categorie["STRESS"] = 3] = "STRESS";
        Categorie[Categorie["ADVIES"] = 4] = "ADVIES";
    })(Categorie || (Categorie = {}));
    ;
    var ScreenID;
    (function (ScreenID) {
        ScreenID[ScreenID["A1"] = 0] = "A1";
        ScreenID[ScreenID["A1_1"] = 20] = "A1_1";
        ScreenID[ScreenID["B1"] = 1] = "B1";
        ScreenID[ScreenID["B1_1"] = 2] = "B1_1";
        ScreenID[ScreenID["B1_2"] = 3] = "B1_2";
        ScreenID[ScreenID["B1_3"] = 4] = "B1_3";
        ScreenID[ScreenID["B2_1"] = 5] = "B2_1";
        ScreenID[ScreenID["B2_2"] = 6] = "B2_2";
        ScreenID[ScreenID["B2_3"] = 7] = "B2_3";
        ScreenID[ScreenID["B2_4"] = 8] = "B2_4";
        ScreenID[ScreenID["B3_1"] = 9] = "B3_1";
        ScreenID[ScreenID["B3_2"] = 10] = "B3_2";
        ScreenID[ScreenID["B3_3"] = 11] = "B3_3";
        ScreenID[ScreenID["B4_1"] = 12] = "B4_1";
        ScreenID[ScreenID["B4_2"] = 13] = "B4_2";
        ScreenID[ScreenID["B4_3"] = 14] = "B4_3";
        ScreenID[ScreenID["B4_4"] = 15] = "B4_4";
        ScreenID[ScreenID["B4_5"] = 16] = "B4_5";
        ScreenID[ScreenID["C1"] = 17] = "C1";
        ScreenID[ScreenID["C1_1"] = 18] = "C1_1";
        ScreenID[ScreenID["C1_2"] = 19] = "C1_2";
        // ScreenID[ScreenID["C1_3"] = 20] = "C1_3";
        ScreenID[ScreenID["Z1"] = 21] = "Z1";
        ScreenID[ScreenID["Z1_1"] = 22] = "Z1_1";
        ScreenID[ScreenID["Y1"] = 23] = "Y1";
        ScreenID[ScreenID["Y1_1"] = 24] = "Y1_1";
        ScreenID[ScreenID["Y1_2"] = 25] = "Y1_2";
        ScreenID[ScreenID["Y1_3"] = 26] = "Y1_3";
        ScreenID[ScreenID["Y1_4"] = 27] = "Y1_4";
        ScreenID[ScreenID["Y2_1"] = 28] = "Y2_1";
        ScreenID[ScreenID["Y2_2"] = 29] = "Y2_2";
        ScreenID[ScreenID["Y2_3"] = 30] = "Y2_3";
        ScreenID[ScreenID["Y2_4"] = 31] = "Y2_4";
        ScreenID[ScreenID["Y3_1"] = 32] = "Y3_1";
        ScreenID[ScreenID["Y3_2"] = 33] = "Y3_2";
        ScreenID[ScreenID["Y3_3"] = 34] = "Y3_3";
        ScreenID[ScreenID["X1"] = 35] = "X1";
        ScreenID[ScreenID["X1_1"] = 36] = "X1_1";
        ScreenID[ScreenID["X1_2"] = 37] = "X1_2";
        ScreenID[ScreenID["X1_3"] = 38] = "X1_3";
        ScreenID[ScreenID["X1_4"] = 39] = "X1_4";
        ScreenID[ScreenID["ADVIES"] = 40] = "ADVIES";
        ScreenID[ScreenID["START"] = 41] = "START";
    })(ScreenID || (ScreenID = {}));
    ;
    var AntwoordID;
    (function (AntwoordID) {
        AntwoordID[AntwoordID["X1"] = 0] = "X1";
        AntwoordID[AntwoordID["X2"] = 1] = "X2";
        AntwoordID[AntwoordID["X3"] = 2] = "X3";
        AntwoordID[AntwoordID["X4"] = 3] = "X4";
        AntwoordID[AntwoordID["X5"] = 4] = "X5";
    })(AntwoordID || (AntwoordID = {}));
    var AdviesID;
    (function (AdviesID) {
        AdviesID[AdviesID["A"] = 0] = "A";
        AdviesID[AdviesID["B"] = 1] = "B";
        AdviesID[AdviesID["C"] = 2] = "C";
        AdviesID[AdviesID["D"] = 3] = "D";
        AdviesID[AdviesID["E"] = 4] = "E";
        AdviesID[AdviesID["F"] = 5] = "F";
        AdviesID[AdviesID["G"] = 6] = "G";
        AdviesID[AdviesID["H"] = 7] = "H";
        AdviesID[AdviesID["I"] = 8] = "I";
        AdviesID[AdviesID["J"] = 9] = "J";
        AdviesID[AdviesID["K"] = 10] = "K";
        AdviesID[AdviesID["L"] = 11] = "L";
        AdviesID[AdviesID["M"] = 12] = "M";
        AdviesID[AdviesID["N"] = 13] = "N";
        AdviesID[AdviesID["O"] = 14] = "O";
        AdviesID[AdviesID["P"] = 15] = "P";
        AdviesID[AdviesID["Q"] = 16] = "Q";
        AdviesID[AdviesID["R"] = 17] = "R";
        AdviesID[AdviesID["NONE"] = 18] = "NONE";
    })(AdviesID || (AdviesID = {}));
    var ProductID;
    (function (ProductID) {
        ProductID[ProductID["A1"] = 0] = "A1";
        ProductID[ProductID["A2"] = 1] = "A2";
        ProductID[ProductID["A3"] = 2] = "A3";
        ProductID[ProductID["A4"] = 3] = "A4";
        ProductID[ProductID["A5"] = 4] = "A5";
        ProductID[ProductID["A6"] = 5] = "A6";
        ProductID[ProductID["A7"] = 6] = "A7";
        ProductID[ProductID["B1"] = 7] = "B1";
        ProductID[ProductID["B2"] = 8] = "B2";
        ProductID[ProductID["B3"] = 9] = "B3";
        ProductID[ProductID["B4"] = 10] = "B4";
        ProductID[ProductID["B5"] = 11] = "B5";
        ProductID[ProductID["C1"] = 12] = "C1";
        ProductID[ProductID["D1"] = 13] = "D1";
        ProductID[ProductID["D2"] = 14] = "D2";
        ProductID[ProductID["D3"] = 15] = "D3";
        ProductID[ProductID["D4"] = 16] = "D4";
        ProductID[ProductID["I1"] = 17] = "I1";
        ProductID[ProductID["K1"] = 18] = "K1";
        ProductID[ProductID["K2"] = 19] = "K2";
        ProductID[ProductID["J1"] = 20] = "J1";
        ProductID[ProductID["L1"] = 21] = "L1";
        ProductID[ProductID["NONE"] = 22] = "NONE";
    })(ProductID || (ProductID = {}));
    ;
    class Antwoord {
        constructor(id, antwoord, subantwoord) {
            this._antwoord = antwoord;
            this._subAntwoord = subantwoord;
            this._uitslag = false;
            this._iD = id;
        }
        get ID() {
            return this._iD;
        }
        get Antwoord() {
            return this._antwoord;
        }
        get SubAntwoord() {
            return this._subAntwoord;
        }
        get Uitslag() {
            return this._uitslag;
        }
        set Uitslag(uitslag) {
            this._uitslag = uitslag;
        }
        Button() {
            var element = document.createElement("div");
            if (this._subAntwoord == null || this._subAntwoord == "") {
                var divTop = document.createElement("div");
                divTop.setAttribute("class", "sectietop");
                divTop.innerHTML = this._antwoord.toUpperCase();
                element.appendChild(divTop);
                element.setAttribute("class", "btnKeuze");
            }
            else {
                var divTop = document.createElement("div");
                var divBottom = document.createElement("div");
                divTop.setAttribute("class", "sectietop");
                divBottom.setAttribute("class", "sectiebottom");
                divTop.textContent = this._antwoord.toUpperCase();
                divBottom.textContent = this._subAntwoord;
                element.appendChild(divTop);
                element.appendChild(divBottom);
                element.setAttribute("class", "btnKeuzeLarge");
            }
            return element;
        }
    }
    class ScreenData {
        constructor(id) {
            this._iD = id;
            this._antwoorden = new HashMap();
        }
        get ID() {
            return this._iD;
        }
        Titel() {
            switch (this._categorie) {
                case Categorie.ALGEMEEN: return "ALGEMENE VRAGEN";
                case Categorie.SLAPEN: return "VRAGEN OVER SLAAP";
                case Categorie.STRESS: return "VRAGEN OVER STRESS";
                case Categorie.SPANNING: return "VRAGEN OVER SPANNING";
                case Categorie.ADVIES: return "ADVIES";
                default: return "ONGELDIGE CATEGORIE";
            }
        }
        get Vraag() {
            return this._vraag;
        }
        set Vraag(vraag) {
            this._vraag = vraag;
        }
        get Categorie() {
            return this._categorie;
        }
        set Categorie(categorie) {
            this._categorie = categorie;
        }
        get Antwoorden() {
            return this._antwoorden;
        }
        set Antwoorden(antwoorden) {
            this._antwoorden = antwoorden;
        }
        Reset() {
            if (this._antwoorden != null && this._antwoorden.length > 0) {
                this._antwoorden.each(antwoord => antwoord.Uitslag = false);
            }
        }
        static GetNextScreen(currentScreen, currentAntwoord) {
            switch (currentScreen) {
                case ScreenID.A1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.A1_1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.Z1;
                case ScreenID.A1_1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.B1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.C1
                case ScreenID.B1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.B1_1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.B2_1;
                    if (currentAntwoord == AntwoordID.X3)
                        return ScreenID.B3_1;
                    if (currentAntwoord == AntwoordID.X4)
                        return ScreenID.B4_1;
                    if (currentAntwoord == AntwoordID.X5)
                        return ScreenID.C1;
                case ScreenID.B1_1:
                    return ScreenID.B1_2;
                case ScreenID.B1_2:
                    return ScreenID.B1_3;
                case ScreenID.B1_3:
                    return ScreenID.ADVIES;
                case ScreenID.B2_1:
                    return ScreenID.B2_2;
                case ScreenID.B2_2:
                    return ScreenID.B2_3;
                case ScreenID.B2_3:
                    return ScreenID.B2_4;
                case ScreenID.B2_4:
                    return ScreenID.ADVIES;
                case ScreenID.B3_1:
                    return ScreenID.B3_2;
                case ScreenID.B3_2:
                    return ScreenID.B3_3;
                case ScreenID.B3_3:
                    return ScreenID.ADVIES;
                case ScreenID.B4_1:
                    return ScreenID.B4_2;
                case ScreenID.B4_2:
                    return ScreenID.B4_3;
                case ScreenID.B4_3:
                    return ScreenID.B4_4;
                case ScreenID.B4_4:
                    return ScreenID.B4_5;
                case ScreenID.B4_5:
                    return ScreenID.C1;
                case ScreenID.C1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.C1_1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.C1_1;
                    if (currentAntwoord == AntwoordID.X3)
                        return ScreenID.ADVIES;
                case ScreenID.C1_1:
                    return ScreenID.C1_2;
                case ScreenID.C1_2:
                    return ScreenID.ADVIES;
                // case ScreenID.C1_3:
                //     return ScreenID.ADVIES;
                case ScreenID.Z1:
                    return ScreenID.Z1_1;
                case ScreenID.Z1_1:
                    return ScreenID.Y1;
                case ScreenID.Y1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.Y1_1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.Y2_1;
                    if (currentAntwoord == AntwoordID.X3)
                        return ScreenID.Y3_1;
                    if (currentAntwoord == AntwoordID.X4)
                        return ScreenID.X1;
                case ScreenID.Y1_1:
                    return ScreenID.Y1_2;
                case ScreenID.Y1_2:
                    return ScreenID.Y1_3;
                case ScreenID.Y1_3:
                    return ScreenID.Y1_4;
                case ScreenID.Y1_4:
                    return ScreenID.X1;
                case ScreenID.Y2_1:
                    return ScreenID.Y2_2;
                case ScreenID.Y2_2:
                    return ScreenID.Y2_3;
                case ScreenID.Y2_3:
                    return ScreenID.Y2_4;
                case ScreenID.Y2_4:
                    return ScreenID.X1;
                case ScreenID.Y3_1:
                    return ScreenID.Y3_2;
                case ScreenID.Y3_2:
                    return ScreenID.Y3_3;
                case ScreenID.Y3_3:
                    return ScreenID.X1;
                case ScreenID.X1:
                    if (currentAntwoord == AntwoordID.X1)
                        return ScreenID.X1_1;
                    if (currentAntwoord == AntwoordID.X2)
                        return ScreenID.X1_1;
                    if (currentAntwoord == AntwoordID.X3)
                        return ScreenID.ADVIES;
                case ScreenID.X1_1:
                    return ScreenID.X1_2;
                case ScreenID.X1_2:
                    return ScreenID.X1_3;
                case ScreenID.X1_3:
                    return ScreenID.X1_4;
                case ScreenID.X1_4:
                    return ScreenID.ADVIES;
            }
            throw "Ongeldig scherm id";
        }
    }
    class AdviesData {
        constructor(id, html, whiteButton) {
            this._iD = id;
            this._html = html;
            this._whiteButton = whiteButton;
        }
        get ID() {
            return this._iD;
        }
        get HTML() {
            return this._html;
        }
        get WhiteButton() {
            return this._whiteButton;
        }
        static getNoneHtml() {
            return "De test wijst uit dat je minder dan gemiddeld moeite lijkt te hebben met inslapen, spanning of nervositeit. Heb je toch het gevoel meer te willen weten over deze onderwerpen? Kijk dan eens op www.dagennachtrust.nl of vraag uw drogist.";
        }
    }
    class ProductData {
        constructor(id) {
            this._iD = id;
            console.log("check")
            switch (this._iD) {
                case AdviesID.A: {
                    this._header = "Sleepzz Original";
                    this._html = "500 smelttabletjes met 0.1 mg pure melatonine";
                    this._image = "Sleepzz-Original.jpg";
                    break;
                }
                case AdviesID.B: {
                    this._header = "Sleepzz Melatonine forte";
                    this._html = "Draagt bij een een gezonde slaap<sup>1</sup><br/><br/>Extra sterke formule met 400 mg <br/>(1)Citroenmelisse.";
                    this._image = "Sleepzz-Melatonine-Forte.jpg";
                    break;
                }
                case AdviesID.C: {
                    this._header = "Sleepzz Slaapfit";
                    this._html = "Fit wakker worden<sup>1</sup>.<br/><br/>Sleepzz Slaapfit bevordert een ontspannen slaap<sup>2</sup> en helpt uitgerust wakker te worden<sup>3</sup>.<br/><br/>Met (1)Passiflora, (2)Slaapmutsje, (3)Vitamine B2 en IJzer.";
                    this._image = "Sleepzz-Slaapfit.jpg";
                    break;
                }
                case AdviesID.D: {
                    this._header = "Sleepzz TimeRelease";
                    this._html = "500 smelttabletjes met 0.1 mg pure melatonine.<br/><br/>Geleidelijke afgifte van melatonine gedurende de nacht.";
                    this._image = "Sleepzz-Time-Release.jpg";
                    break;
                }
                case AdviesID.E: {
                    this._header = "Sleepzz Magnesium";
                    this._html = "813 mg magnesiumcitraat.<br/><br/>Magnesium en vitamine B6 helpen bij vermoeidheid en moeheid.<br/><br/>Passiflora-extract draagt bij aan een natuurlijke slaap.";
                    this._image = "Sleepzz-Magnesium.jpg";
                    break;
                }
                case AdviesID.F: {
                    this._header = "Melatomatine Time Release met hop";
                    this._html = "Melatonine met 500 mg Hop.<br/><br/>Hop draagt bij aan een gezonde slaap.<br/><br/>Wordt geleidelijk aan het lichaam afgegeven.";
                    this._image = "Melatomatine-Time-Release-met-Hop.jpg";
                    break;
                }
                case AdviesID.G: {
                    this._header = "Valdispert Nacht Extra Sterk";
                    this._html = "Met Valeriaan (voor rust) en Passiflora (slaap).";
                    this._image = "valdispert-nacht-extra-sterk-melatonine_2.jpg";
                    break;
                }
                // case AdviesID.H: {
                //     this._header = "Valdispert Stress Moments";
                //     this._html = "Met Valeriaan (voor rust) en Citroenmelisse (tegen zenuwachtig gevoel).";
                //     this._image = "Packshot-Valdispert-Stress-Moments-tablet-voor-pers-01-2009-300-dpi.jpg";
                //     break;
                // }
                case AdviesID.I: {
                    this._header = "Valdispert Stress Moments Extra sterk";
                    this._html = "Bij stress situaties zoals examens. Helpt bij een zenuwachtig gevoel<sup>1</sup>.<br/><br/>Kalmeert en stelt gerust.<br/><br/>Met Ginseng, dat de concentratie verbetert en met (1)Citroenmelisse.";
                    this._image = "valdispert-stress-moments-extrasterk-20tabs_3.jpg";
                    break;
                }
                case AdviesID.J: {
                    this._header = "Valdispert 450 mg";
                    this._html = "Verlicht nervositeit en bevordert een ontspannen slaap.<br/><br/>Valdispert 450 mg is 100% natuurlijk, geeft geen gewenning en kan langere tijd gebruikt worden.<br/><br/>Geregistreerd geneesmiddel. Lees voor gebruik de bijsluiter.";
                    this._image = "valdispert-450mg-40x_3.jpg";
                    break;
                }
                case AdviesID.K: {
                    this._header = "Valdispert Rust";
                    this._html = "Met valeriaan.<br/><br/>Ontspannend en rustgevend.";
                    this._image = "valdispert-rust_3.jpg";
                    break;
                }
                case AdviesID.L: {
                    this._header = "Valdispert Rust Extra Sterk";
                    this._html = "Met valeriaan.<br/><br/>Geeft rust bij extra spanning en bevordert je concentratievermogen.";
                    this._image = "valdispert-rust-extra-sterk_3.jpg";
                    break;
                }
                case AdviesID.O: {
                    this._header = "Valdispert Kids Rust";
                    this._html = "Valdispert Kids Rust heeft een rustgevende en ontspannende invloed overdag.<br/><br/>Daarnaast bevordert het een natuurlijke, gezonde nachtrust.";
                    this._image = "Packshot-Valdispert-Kids-Rust-met-fles-rechts-300-dpi-02-2008.jpg";
                    break;
                }
                // case AdviesID.P: {
                //     this._header = "Valdispert 45 mg";
                //     this._html = "Met valeriaanwortel.<br/>Helpt bij lichte vormen van nervositeit en spanning.<br/><br/>Ook geschikt voor kinderen vanaf 12 jaar.<br/><br/>Geregistreerd geneesmiddel. Lees voor gebruik de bijsluiter.";
                //     this._image = "valdispert-45mg_2.jpg";
                //     break;
                // }
                case AdviesID.Q: {
                    this._header = "Sleepzz Junior Rust- en Slaapsiroop";
                    this._html = "Rustgevend bij druk gedrag.<br/><br/>Bevordert een natuurlijke gezonde slaap.";
                    this._image = "Packshot-Sleepzz-Junior-150-ml-front-01-2012-300-dpi.jpg";
                    break;
                }
                case AdviesID.R: {
                    this._header = "Sleepzz Junior of Drink & Dream Kids";
                    this._html = "<b>Sleepzz Junior Rust- en Slaapsiroop</b><br/>Rustgevend bij druk gedrag.<br/>Bevordert een natuurlijke gezonde slaap.<br/><br/><b>Drink & Dream Kids</b><br/>Ontspant en bevordert een natuurlijke slaap<sup>1</sup>.<br/>Op basis van Citroenmelisse, Linde en (1)Kamille.";
                    this._image = "SleepzzJunior_D&D-kids.jpg";
                    break;
                }
            }
        }
        get ID() {
            return this._iD;
        }
        get Header() {
            return this._header;
        }
        get Html() {
            return this._html;
        }
        get Image() {
            return this._image;
        }
    }
    class ProductCarousselData {
        constructor(id) {
            this._iD = id;
            console.log("Product ID: ",id)
            switch (this._iD) {
                // case ProductID.A7: {
                //     this._header = "Melatomatine Pure Melatonine";
                //     this._html = "Makkelijk te doseren, geeft geen gewenning";
                //     this._ondertitel = "<sup>1</sup>0,1 melatonine";
                //     this._image = "Melatomatine Pure Melatonine.png";
                //     break;
                // }
                case ProductID.A1: {
                    this._header = "Advies Kaart!";
                    // this._html = "Makkelijk te doseren, geeft geen gewenning";
                    // this._ondertitel = "<sup>1</sup>0,1 melatonine";
                    this._image = "DA_adviespagina.jpg";
                    break;
                }
                // case ProductID.A2: {
                //     this._header = "Sleepzz Melatonine Time Release";
                //     this._html = "Time relase, makkelijk te doseren, geeft geen gewenning";
                //     this._ondertitel = "<sup>1</sup>0,1 melatonine time release";
                //     this._image = "8711744052300_7.png";
                //     break;
                // }
                // case ProductID.A3: {
                //     this._header = "Sleepzz Original Plantaardige Melatonine";
                //     this._html = "Makkelijk te doseren, geeft geen gewenning.";
                //     this._ondertitel = "<sup>1</sup>0,1 plantaardige melatonine";
                //     this._image = "Sleepzz Original Plantaardige Melatonine.png";
                //     break;
                // }
                // case ProductID.A4: {
                //     this._header = "Sleepzz slaapmutsje en plantaardige Melatonine";
                //     this._html = "Melatonine 100% uit planten, geeft geen gewenning";
                //     this._ondertitel = "<sup>1</sup>Slaapmutsje ondersteunt je natuurlijke slaap";
                //     this._image = "Sleepzz Slaapmutsje en Plantaardige Melatonine2.png";
                //     break;
                // }
                // case ProductID.A5: {
                //     this._header = "Valdispert Herbatonine plantaardige melatonine en slaapmutsje";
                //     this._html = "Vrij van geur- en smaakstoffen, bevat geen conserveermiddelen, veilig in gebruik";
                //     this._ondertitel = "<sup>1</sup>Plantaardige Melatonine uit Alfalfa, Chlorella en Rijstplanten<br/>Ondersteunt je natuurlijke slaap door Slaapmutsje";
                //     this._image = "Valdispert Herbatonine Plant.Mela en Slaapmutsje.png";
                //     break;
                // }
                // case ProductID.A6:
                // case ProductID.B5: {
                //     this._header = "Sleepzz Gezonde Nachtrust";
                //     this._html = "3-voudige slaapformule met lavendelolie en slaapmutsje, geeft geen gewenning";
                //     this._ondertitel = "<sup>1</sup>3-voudige slaapformule: Geeft ontspanning voor het slapen door lavendel*<br/>Ondersteunt gezonde slaap door lavendel*<br/>Helpt uitgerust wakker worden door slaapmutsje*";
                //     this._image = "Sleepzz Gezonde Nachtrust.jpg";
                //     break;
                // }
                // case ProductID.B3: {
                //     this._header = "Sleepzz CBD Cannabidiol 7mg en Melatonine";
                //     this._html = "Hoge kwaliteit CBD, geeft geen gewenning, time release";
                //     this._ondertitel = "<sup>1</sup>Ondersteund de kwaliteit van je slaap door slaapmutsje";
                //     this._image = "Sleepzz CBD Slaapmutsje en Plantaardige Melatonine.png";
                //     break;
                // }
                case ProductID.B1: {
                    this._header = "Valdispert 450 mg";
                    this._html = "Verlicht nervositeit en bevordert een ontspannen slaap.<br>Kruidengeneesmiddel. Werkzame stof: valeriaan. Lees voor het gebruik de bijsluiter.";
                    this._ondertitel = "<sup>1</sup>Bij nervositeit en slaapmoeilijkheden.";
                    this._image = "valdispert-450mg-40x_3.jpg";
                    break;
                }
                // case ProductID.B2: {
                //     this._header = "Shiepz Melatonine 5mg";
                //     this._html = "Hoge dosering melatonine.";
                //     this._ondertitel = "1 tablet met water innemen.";
                //     this._image = "Shiepz Melatonine 5mg.jpg";
                //     break;
                // }
                // case ProductID.B4: {
                //     this._header = "Sleepzz Power Sleep";
                //     this._html = "Slaapformule die je ondersteunt bij het slapen";
                //     this._ondertitel = "<sup>1</sup>Slaapformule voor behoud van je natuurlijke slaap en ondersteund de kwaliteit van slapen*";
                //     this._image = "Sleepzz Power Sleep.jpg";
                //     break;
                // }
                case ProductID.C1: {
                    this._header = "Advies Kaart!";
                    // this._html = "Voor een gezonde slaap, helpt uitgerust wakker te worden, met B vitaminen, uniek dubbellaags tablet, Time release";
                    // this._ondertitel = "<sup>1</sup>Fit wakker worden";
                    this._image = "DA_adviespagina.jpg";
                    break;
                }
                // case ProductID.D2: {
                //     this._header = "Valdispert Nacht Extra Sterk";
                //     this._html = "Extra Sterk. Op basis van valeriaanwortel, passiebloem en melatonine";
                //     this._ondertitel = "<sup>1</sup>Bevordert een natuurlijke nachtrust en gezonde slaap";
                //     this._image = "valdispert-nacht-extra-sterk-melatonine_3.jpg";
                //     break;
                // }
                // case ProductID.D3: {
                //     this._header = "Valdispert Nacht Melatonine 5-HTP L-Tryptofaan";
                //     this._html = "Op basis van valeriaanwortel, melatonine, slaapmutsje en B vitaminen";
                //     this._ondertitel = "<sup>1</sup>Krachtige formule voor een natuurlijke nachtrust en behoud van je natuurlijke slaap";
                //     this._image = "Valdispert Nacht Melatonine 5-HTP L-Tryptofaan.jpg";
                //     break;
                // }
                case ProductID.D1: {
                    this._header = "Advies Kaart!";
                    // this._html = "120mg valeriaanwortelextract, 0,29mg melatonine, 50mg slaapmutsje en B vitaminen";
                    // this._ondertitel = "<sup>1</sup>Krachtige formule voor een natuurlijke nachtrust en behoud van je natuurlijke slaap";
                    this._image = "DA_adviespagina.jpg";
                    break;
                }
                case ProductID.I1: {
                    this._header = "Valdispert Stress Moments Forte Melisse";
                    this._html = "Ontspant bij stress situaties (citroenmelisse) en is goed voor concentratie (valeriaanwortel). <br>Evaluatie gezondheidsclaims is lopende";
                    this._ondertitel = "<sup>1</sup>met 449 mg valeriaanwortel en 400 mg citroenmelisse.";
                    this._image = "valdispert-stress-moments-extrasterk-20tabs_4.png";
                    break;
                }
                case ProductID.K1: {
                    this._header = "Valdispert Rust";
                    this._html = "Onspannend en rustgevend door valeriaanwortelextract.<br>Evaluatie gezondheidsclaim is lopende";
                    this._ondertitel = "";
                    this._image = "valdispert-rust_3.jpg";
                    break;
                }
                // case ProductID.D4:
                // case ProductID.K2: {
                //     this._header = "Valdispert Dag & Nacht";
                //     this._html = "Op basis van valeriaanwortelextract, rhodiola, melatonine en kamille.<br/>Handig; een tablet voor overdag en voor de nacht in 1 verpakking. Geeft geen gewenning";
                //     this._ondertitel = "<sup>1</sup>Overdag: ontspannend, rustgevend*<br/>Voor de nacht: bevordert een goede en nachtrust";
                //     this._image = "Valdispert Dag & Nacht.jpg";
                //     break;
                // }
                case ProductID.J1: {
                    this._header = "Valdispert 450 mg";
                    this._html = "dsfg";
                    this._ondertitel = "<sup>1</sup>Bij nervositeit en slaapmoeilijkheden.";
                    this._image = "valdispert-450mg-40x_3.jpg";
                    break;
                }
                case ProductID.L1: {
                    this._header = "Advies Kaart!";
                    // this._html = "Extra Sterk";
                    // this._ondertitel = "<sup>1</sup>Onspannend en rustgevend";
                    this._image = "DA_adviespagina.jpg";
                    break;
                }
                case ProductID.NONE: {
                    this._header = "De test wijst uit dat je minder dan gemiddeld moeite lijkt te hebben met inslapen, spanning of nervositeit. ";
                    this._html = "Heb je toch het gevoel meer te willen weten over deze onderwerpen?<br/> Kijk dan eens op <a href=\"https://www.da.nl/blog/niet-slapen-tips\" target=\"_blank\">https://www.da.nl/blog/niet-slapen-tips</a> of vraag uw drogist.";
                    this._ondertitel = "";
                    this._image = "";
                    break;
                }
            }
        }
        get ID() {
            return this._iD;
        }
        get Header() {
            return this._header;
        }
        get Html() {
            return this._html;
        }
        get Ondertitel() {
            return this._ondertitel;
        }
        get Image() {
            return this._image;
        }
    }
    class Test extends BaseScreen {
        constructor() {
            super('test');
            this._filledScreens = new Array();
            this.fillScreenData();
            this.reset();
            $('#btnClose').click(e => this.closeEvent(e));
            $('#btnBack').click(e => this.backEvent(e));
        }
        fillScreenData() {
            this._testScreens = new HashMap();
            var screen = new ScreenData(ScreenID.A1);
            {
                screen.Categorie = Categorie.ALGEMEEN;
                screen.Vraag = "Voor wie doe je de test?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Voor mezelf", "Je moet 18 jaar of ouder zijn"));
                // screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Voor mijn kind", "Voor kinderen van 3 tot 18 jaar"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.A1_1)
            {
                screen.Categorie = Categorie.ALGEMEEN;
                screen.Vraag = "Waar wil je de test voor doen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Slaapproblemen"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Nerveuze gevoelens"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Wat voor slaapproblemen heb je? Eén categoriemogelijkheid. Kies de categorie waar je het meeste last van ondervindt.";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ik heb moeite met inslapen"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Ik heb moeite met doorslapen"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Ik slaap genoeg, maar word toch moe wakker"));
                // screen.Antwoorden.add(AntwoordID.X5.toString(), new Antwoord(AntwoordID.X5, "Ik heb geen slaapproblemen"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B1_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel uur lig je gemiddeld wakker voordat je in slaap valt?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan een half uur"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Tussen een half uur en een uur"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Meer dan een uur"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B1_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel nachten per week heb je gemiddeld moeite met inslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 nacht"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 nachten"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer nachten"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B1_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang heb je al moeite met inslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                // screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken")); 
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B2_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Waarom word je 's nachts wakker?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ik ben een lichte slaper"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Ik voel me onrustig"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Ik moet naar het toilet"));
                screen.Antwoorden.add(AntwoordID.X5.toString(), new Antwoord(AntwoordID.X5, "Een andere reden"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B2_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang ben je 's nachts in totaal wakker?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan een half uur"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Tussen een half uur en anderhalf uur"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Meer dan anderhalf uur"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B2_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel nachten per week heb je gemiddeld moeite met doorslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 nacht"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 nachten"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer nachten"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B2_4);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang heb je al moeite met doorslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B4_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel ochtenden per week word je gemiddeld moe wakker, ook al slaap je goed?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 ochtend"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 ochtenden"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer ochtenden"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B4_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang word je al moe wakker, ook al slaap je goed?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B4_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe vaak maak je vlak voordat je naar bed gaat nog gebruik van tablet, computer of tv?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan 1 keer per week"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "1-2 keer per week"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3-4 keer per week"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Meer dan 5 keer per week"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B4_4);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Heb je wel eens last van jetlag?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Nee"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.B4_5);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Werk je onregelmatige tijden, bijvoorbeeld nachtdiensten?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Nee"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.C1);
            {
                screen.Categorie = Categorie.STRESS;
                screen.Vraag = "Voel jij je wel eens nerveus?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Enigszins"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Nee"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.C1_1);
            {
                screen.Categorie = Categorie.STRESS;
                screen.Vraag = "Wat is de reden dat je je weleens nerveus voelt?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Er komt een belangrijk moment aan"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Ik zit in een stressvolle periode in mijn leven"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Ik ben van nature zenuwachtig aangelegd"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Weet ik niet"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.C1_2);
            {
                screen.Categorie = Categorie.STRESS;
                screen.Vraag = "Hoe sterk zijn je gevoelens?";
                // screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Heel sterk"));
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Sterk"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Enigszins sterk"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Niet echt sterk"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            // var screen = new ScreenData(ScreenID.C1_3);
            // {
            //     screen.Categorie = Categorie.STRESS;
            //     screen.Vraag = "Hoe lang houden de gevoelens van spanning en/of nervositeit aan?";
            //     screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan 1 week"));
            //     screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "1 tot 3 weken"));
            //     screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Meer dan 3 weken"));
            // }
            // this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Z1);
            {
                screen.Categorie = Categorie.ALGEMEEN;
                screen.Vraag = "Hoe oud is je kind?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Tussen 3 en 12 jaar"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Tussen 12 en 18 jaar"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Z1_1);
            {
                screen.Categorie = Categorie.ALGEMEEN;
                screen.Vraag = "Krijgt je kind veel lichaamsbeweging?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja, best veel"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Gemiddeld"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Nee, vrij weinig"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Wat voor slaapproblemen heeft je kind? Kies de categorie waar je kind het meeste last van ondervindt.";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Moeite met inslapen"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Moeite met doorslapen"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Veel te vroeg wakker worden"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Geen slaapproblemen"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y1_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Waarom kan je kind niet in slaap vallen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Hij/zij is gespannen"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Hij/zij heeft teveel energie"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Een andere reden"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Hij/zij is bang"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y1_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel uur ligt je kind je 's nachts gemiddeld wakker voordat hij/zij in slaap valt?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan een half uur"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Tussen een half uur en anderhalf uur"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Meer dan anderhalf uur"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y1_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel nachten per week heeft je kind gemiddeld moeite met inslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 nacht"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 nachten"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer nachten"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y1_4);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang heeft je kind al moeite met inslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y2_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Waarom wordt je kind 's nachts wakker?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Hij/zij is een lichte slaper"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Te lichte kamer"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Een andere reden"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Bedplassen"));
                screen.Antwoorden.add(AntwoordID.X5.toString(), new Antwoord(AntwoordID.X5, "Hij/zij is bang en/of nachtmerries"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y2_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe vaak wordt je kind 's nachts ongeveer wakker?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 keer"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 keer"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer keer"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y2_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel nachten per week heeft je kind gemiddeld moeite met doorslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 nacht"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 nachten"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer nachten"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y2_4);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang heeft je kind al moeite met doorslapen?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y3_1);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoeveel uur te vroeg wordt je kind gemiddeld wakker in de ochtend?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Minder dan een half uur"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Tussen een half uur en een uur"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Meer dan een uur"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y3_2);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Gemiddeld hoeveel dagen per week wordt je kind te vroeg wakker?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 dag"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 dagen"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "3 of meer dagen"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.Y3_3);
            {
                screen.Categorie = Categorie.SLAPEN;
                screen.Vraag = "Hoe lang heeft je kind al last van te vroeg wakker worden?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "minder dan 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "2 tot 8 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.X1);
            {
                screen.Categorie = Categorie.SPANNING;
                screen.Vraag = "Heeft je kind de afgelopen tijd last van spanning en/of nervositeit?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Af en toe"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Nee"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.X1_1);
            {
                screen.Categorie = Categorie.SPANNING;
                screen.Vraag = "Wat is de reden dat hij/zij spanning en/of nervositeit ervaart?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Er komt een belangrijk moment aan"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Hij/zij zit in een stressvolle periode (familie / school)"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Dit zit in <zijn/haar> persoonlijkheid"));
                screen.Antwoorden.add(AntwoordID.X4.toString(), new Antwoord(AntwoordID.X4, "Weet ik niet"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.X1_2);
            {
                screen.Categorie = Categorie.SPANNING;
                screen.Vraag = "Hoe lang heeft je kind deze gevoelens al?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.X1_3);
            {
                screen.Categorie = Categorie.SPANNING;
                screen.Vraag = "Is je kind de afgelopen tijd drukker en/of nerveuzer dan normaal?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "Ja"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "Een klein beetje"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "Nee"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.X1_4);
            {
                screen.Categorie = Categorie.SPANNING;
                screen.Vraag = "Hoe lang is je kind al zo?";
                screen.Antwoorden.add(AntwoordID.X1.toString(), new Antwoord(AntwoordID.X1, "1 of 2 weken"));
                screen.Antwoorden.add(AntwoordID.X2.toString(), new Antwoord(AntwoordID.X2, "3 tot 7 weken"));
                screen.Antwoorden.add(AntwoordID.X3.toString(), new Antwoord(AntwoordID.X3, "8 of meer weken"));
            }
            this._testScreens.add(screen.ID.toString(), screen);
            var screen = new ScreenData(ScreenID.ADVIES);
            {
                screen.Categorie = Categorie.ADVIES;
                screen.Vraag = "";
            }
            this._testScreens.add(screen.ID.toString(), screen);
        }
        resetScreen() {
            $("#catergorie2Line").css('visibility', 'hidden');
            $("#catergorie3Line").css('visibility', 'hidden');
            $("#catergorie4Line").css('visibility', 'hidden');
            $("#catergorie1Circle").css('visibility', 'hidden');
            $("#catergorie2Circle").css('visibility', 'hidden');
            $("#catergorie3Circle").css('visibility', 'hidden');
            $("#catergorie4Circle").css('visibility', 'hidden');
            $("#catergorie1").removeClass('whiteText');
            $("#catergorie2").removeClass('whiteText');
            $("#catergorie3").removeClass('whiteText');
            $("#catergorie4").removeClass('whiteText');
            $("#catergorie1").html("");
            $("#catergorie2").html("");
            $("#catergorie3").html("");
            $("#catergorie4").html("");
            this.setCategorie();
        }
        setCategorie() {
            var data = this._testScreens.get(ScreenID.A1);
            var antwoordKids = data.Antwoorden.get(AntwoordID.X2);
            if (antwoordKids != undefined && antwoordKids.Uitslag) {
                this.fillKids();
            }
            else {
                this.fillVolwassene();
            }
        }
        fillVolwassene() {
            $("#catergorie1").html(Categorie[Categorie.ALGEMEEN]);
            $("#catergorie2").html(Categorie[Categorie.SLAPEN]);
            $("#catergorie3").html(Categorie[Categorie.STRESS]);
            $("#catergorie4").html(Categorie[Categorie.ADVIES]);
        }
        fillKids() {
            $("#catergorie1").html(Categorie[Categorie.ALGEMEEN]);
            $("#catergorie2").html(Categorie[Categorie.SLAPEN]);
            $("#catergorie3").html(Categorie[Categorie.SPANNING]);
            $("#catergorie4").html(Categorie[Categorie.ADVIES]);
        }
        fillScreen(screendata, left) {
            var div = left ? $('#leftContent') : $('#rightContent');
            if (left == null || left) {
                $("#screenTitle", div).html(screendata.Titel());
                switch (screendata.Categorie) {
                    case Categorie.ADVIES:
                        $("#catergorie4Circle", div).css('visibility', 'visible');
                        $("#catergorie4Line", div).css('visibility', 'visible');
                        $("#catergorie4", div).addClass('whiteText');
                    case Categorie.STRESS:
                    case Categorie.SPANNING:
                        $("#catergorie3Circle", div).css('visibility', 'visible');
                        $("#catergorie3Line", div).css('visibility', 'visible');
                        $("#catergorie3", div).addClass('whiteText');
                    case Categorie.SLAPEN:
                        $("#catergorie2Circle", div).css('visibility', 'visible');
                        $("#catergorie2Line", div).css('visibility', 'visible');
                        $("#catergorie2", div).addClass('whiteText');
                    case Categorie.ALGEMEEN:
                        $("#catergorie1Circle", div).css('visibility', 'visible');
                        $("#catergorie1", div).addClass('whiteText');
                }
                $('#movableLeft', div).html(screendata.Vraag);
            }
            if (left == null || !left) {
                if (screendata.ID == ScreenID.A1) {
                    $("#btnBack", div).hide();
                }
                else {
                    $("#btnBack", div).show();
                }
                $('#movableRight', div).empty();
                screendata.Antwoorden.each(t => {
                    var valid = true;
                    var antwoord = t;
                    if (screendata.ID == ScreenID.Y1_1 || screendata.ID == ScreenID.Y2_1) {
                        var B1 = this._testScreens.get(ScreenID.Z1);
                        var B1_X2 = B1.Antwoorden.get(AntwoordID.X2);
                        if ((antwoord.ID == AntwoordID.X4 || antwoord.ID == AntwoordID.X5) && B1_X2.Uitslag) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        var button = antwoord.Button();
                        $('#movableRight', div).append(button);
                        $(button).click(e => this.buttonClicked(antwoord, e));
                    }
                });
            }
        }
        buttonClicked(antwoord, e) {
            console.log(antwoord)
            e.preventDefault();
            antwoord.Uitslag = true;
            var data = this._testScreens.get(this._currentScreen.toString());
            if (data.ID == ScreenID.A1) {
                this.resetScreen();
            }
            this._filledScreens.push(data);
            this._currentScreen = ScreenData.GetNextScreen(this._currentScreen, antwoord.ID);
            if (this._currentScreen != ScreenID.ADVIES) {
                var newdata = this._testScreens.get(this._currentScreen.toString());
                this.swapAllContent(newdata, false);
            }
            else {
                Screens.getInstance().showAdviesScreen();
            }
            e.stopPropagation();
        }
        closeEvent(e) {
            e.preventDefault();
            this.close();
            e.stopPropagation();
        }
        close() {
            this.reset();
            Screens.getInstance().showStartScreen();
        }
        reset() {
            this._filledScreens = [];
            this._currentScreen = ScreenID.A1;
            this._testScreens.each(data => {
                if (data == null) {
                    throw "huh?";
                }
                data.Reset();
            });
            this.resetScreen();
            var data = this._testScreens.get(this._currentScreen.toString());
            this.swapAllContent(data, true);
        }
        backEvent(e) {
            e.preventDefault();
            this.back();
            e.stopPropagation();
        }
        back() {
            var data = this._filledScreens.pop();
            if (data == null) {
                return;
            }
            data.Reset();
            this._currentScreen = data.ID;
            this.resetScreen();
            this.swapAllContent(data, true);
        }
        swapAllContent(data, reverse) {
            this.swapContent(data, true, reverse);
            this.swapContent(data, false, reverse);
        }
        swapContent(data, left, reverse) {
            var movable = left ? $("#movableLeft") : $("#movableRight");
            var parent = movable.parent();
            var width = (parent.width() + movable.width()) / 2;
            var marginOne = (reverse ? '+' : '-') + '=' + width + 'px';
            var marginTwo = (reverse ? '-' : '+') + '=' + width + 'px';
            var margin2One = (reverse ? '+' : '-') + '=' + width * 2 + 'px';
            var margin2Two = (reverse ? '-' : '+') + '=' + width * 2 + 'px';
            movable.animate({
                'marginLeft': marginOne,
                'marginRight': marginTwo,
            }, 'fast', () => {
                movable.empty();
                this.fillScreen(data, left);
                movable.css('marginLeft', margin2Two);
                movable.css('marginRight', margin2One);
                movable.animate({
                    'marginLeft': marginOne,
                    'marginRight': marginTwo,
                }, 'fast');
            });
        }
        getAdvies() {
            try {
                var advies = new Array();
                var A1 = this._testScreens.get(ScreenID.A1);
                var A1_X1 = A1.Antwoorden.get(AntwoordID.X1);
                var A1_X2 = A1.Antwoorden.get(AntwoordID.X2);
                if (A1_X1.Uitslag) {
                    var B1 = this._testScreens.get(ScreenID.B1);
                    var B1_X1 = B1.Antwoorden.get(AntwoordID.X1);
                    var B1_X2 = B1.Antwoorden.get(AntwoordID.X2);
                    var B1_X4 = B1.Antwoorden.get(AntwoordID.X4);
                    var B1_1 = this._testScreens.get(ScreenID.B1_1);
                    var B1_1_X1 = B1_1.Antwoorden.get(AntwoordID.X1);
                    var B1_2 = this._testScreens.get(ScreenID.B1_2);
                    var B1_2_X1 = B1_2.Antwoorden.get(AntwoordID.X1);
                    var B1_3 = this._testScreens.get(ScreenID.B1_3);
                    var B1_3_X1 = B1_3.Antwoorden.get(AntwoordID.X1);
                    var B2_3 = this._testScreens.get(ScreenID.B2_3);
                    var B2_3_X1 = B2_3.Antwoorden.get(AntwoordID.X1);
                    var B2_4 = this._testScreens.get(ScreenID.B2_4);
                    var B2_4_X1 = B2_4.Antwoorden.get(AntwoordID.X1);
                    var B4_1 = this._testScreens.get(ScreenID.B4_1);
                    var B4_1_X1 = B4_1.Antwoorden.get(AntwoordID.X1);
                    var B4_5 = this._testScreens.get(ScreenID.B4_5);
                    var B4_5_X1 = B4_5.Antwoorden.get(AntwoordID.X1);
                    var B4_5_X2 = B4_5.Antwoorden.get(AntwoordID.X2);
                    var C1 = this._testScreens.get(ScreenID.C1);
                    var C1_X1 = C1.Antwoorden.get(AntwoordID.X1);
                    var C1_X2 = C1.Antwoorden.get(AntwoordID.X2);
                    var C1_2 = this._testScreens.get(ScreenID.C1_2);
                    var C1_2_X1 = C1_2.Antwoorden.get(AntwoordID.X1);
                    var C1_2_X2 = C1_2.Antwoorden.get(AntwoordID.X2);
                    var C1_2_X3 = C1_2.Antwoorden.get(AntwoordID.X3);
                    var meerDanGemiddeld = false;
                    if (B1_X1.Uitslag && B1_1_X1.Uitslag && B1_2_X1.Uitslag && B1_3_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.A, "De test wijst uit dat je minder dan gemiddeld moeite lijkt te hebben met inslapen.<br />Als je tóch een product wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (B1_X1.Uitslag) {
                        meerDanGemiddeld = true;
                        advies.push(new AdviesData(AdviesID.B, "De test wijst uit dat je meer dan gemiddeld moeite lijkt te hebben met inslapen.<br />Kijk hier welk product bij je past.", true));
                    }
                    if (B1_X2.Uitslag && B2_3_X1.Uitslag && B2_4_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.D, "De test wijst uit dat je minder dan gemiddeld moeite lijkt te hebben met doorslapen.<br />Als je tóch een product wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (B1_X2.Uitslag) {
                        meerDanGemiddeld = true;
                        advies.push(new AdviesData(AdviesID.D, "De test wijst uit dat je meer dan gemiddeld moeite lijkt te hebben met doorslapen.<br />Kijk hier welk product bij je past.", true));
                    }
                    if (B1_X4.Uitslag && B4_1_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.C, "De test wijst uit dat je minder dan gemiddeld moeite lijkt te hebben met vermoeid wakker worden.<br/>Als je tóch een product wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (B1_X4.Uitslag) {
                        meerDanGemiddeld = true;
                        advies.push(new AdviesData(AdviesID.C, "De test wijst uit dat je meer dan gemiddeld moeite lijkt te hebben met vermoeid wakker worden.<br/>Kijk hier welk product bij je past.", true));
                    }
                    if (C1_X1.Uitslag || C1_X2.Uitslag) {
                        console.log(B4_5)
                        console.log(B4_5_X1.uitslag)
                        console.log(B4_5_X2.uitslag)
                        if(B4_5_X1.uitslag || B4_5_X2.uitslag){
                            advies.push(new AdviesData(AdviesID.C, "De test wijst uit dat je meer dan gemiddeld moeite lijkt te hebben met vermoeid wakker worden.<br/>Kijk hier welk product bij je past.", true));
                        }
                        else if (meerDanGemiddeld) {
                            while (advies.length > 0) {
                                advies.pop();
                            }
                            if (C1_2_X3.Uitslag) {
                                advies.push(new AdviesData(AdviesID.L, "De test wijst uit dat je wel eens gevoelens kent van spanning of nervositeit, die mogelijk ook van invloed zijn op je slaapgedrag.<br/>Kijk hier welk product bij je past.", true));
                            }
                            else {
                                advies.push(new AdviesData(AdviesID.L, "De test wijst uit dat je wel eens gevoelens kent van spanning of nervositeit, die mogelijk ook van invloed zijn op je slaapgedrag.<br/>Kijk hier welk product bij je past.", true));
                            }
                        }
                        else {
                            if (C1_2_X2.Uitslag) {
                                advies.push(new AdviesData(AdviesID.I, "De test wijst uit dat je wel eens gevoelens kent van spanning of nervositeit.<br/>Kijk hier welk product bij je past.", true));
                            }
                            else if (C1_2_X3.Uitslag) {
                                advies.push(new AdviesData(AdviesID.K, "De test wijst uit dat je wel eens gevoelens kent van spanning of nervositeit.<br/>Kijk hier welk product bij je past.", true));
                            }
                            else {
                                advies.push(new AdviesData(AdviesID.C, "De test wijst uit dat je wel eens gevoelens kent van spanning of nervositeit, die mogelijk ook van invloed zijn op je slaapgedrag.<br/>Kijk hier welk product bij je past.", true));
                            }
                        }
                    }
                    if (advies.length == 0) {
                        advies.push(new AdviesData(AdviesID.NONE, AdviesData.getNoneHtml(), false));
                    }
                }
                else {
                    var B1 = this._testScreens.get(ScreenID.Z1);
                    var B1_X1 = B1.Antwoorden.get(AntwoordID.X1);
                    var B1_X2 = B1.Antwoorden.get(AntwoordID.X2);
                    var C1 = this._testScreens.get(ScreenID.Y1);
                    var C1_X1 = C1.Antwoorden.get(AntwoordID.X1);
                    var C1_X2 = C1.Antwoorden.get(AntwoordID.X2);
                    var C1_X3 = C1.Antwoorden.get(AntwoordID.X3);
                    var C1_1 = this._testScreens.get(ScreenID.Y1_1);
                    var C1_1_X1 = C1_1.Antwoorden.get(AntwoordID.X1);
                    var C1_2 = this._testScreens.get(ScreenID.Y1_2);
                    var C1_2_X1 = C1_2.Antwoorden.get(AntwoordID.X1);
                    // var C1_3 = this._testScreens.get(ScreenID.Y1_3);
                    // var C1_3_X1 = C1_3.Antwoorden.get(AntwoordID.X1);
                    var C1_4 = this._testScreens.get(ScreenID.Y1_4);
                    var C1_4_X1 = C1_4.Antwoorden.get(AntwoordID.X1);
                    // var C2_3 = this._testScreens.get(ScreenID.Y2_3);
                    // var C2_3_X1 = C2_3.Antwoorden.get(AntwoordID.X1);
                    var C2_4 = this._testScreens.get(ScreenID.Y2_4);
                    var C2_4_X1 = C2_4.Antwoorden.get(AntwoordID.X1);
                    var C3_1 = this._testScreens.get(ScreenID.Y3_1);
                    var C3_1_X3 = C3_1.Antwoorden.get(AntwoordID.X3);
                    var C3_2 = this._testScreens.get(ScreenID.Y3_2);
                    var C3_2_X1 = C3_2.Antwoorden.get(AntwoordID.X1);
                    // var C3_3 = this._testScreens.get(ScreenID.Y3_3);
                    // var C3_3_X1 = C3_3.Antwoorden.get(AntwoordID.X1);
                    var D1 = this._testScreens.get(ScreenID.X1);
                    var D1_X1 = D1.Antwoorden.get(AntwoordID.X1);
                    var D1_X2 = D1.Antwoorden.get(AntwoordID.X2);
                    var D1_4 = this._testScreens.get(ScreenID.X1_4);
                    var D1_4_X1 = D1_4.Antwoorden.get(AntwoordID.X1);
                    // && C1_3_X1.Uitslag
                    if (C1_X1.Uitslag && C1_1_X1.Uitslag && C1_2_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je kind minder dan gemiddeld moeite lijkt te hebben met inslapen.<br/>Als je tóch een product voor je kind wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (C1_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je meer dan gemiddeld moeite lijkt te hebben met inslapen.<br />Kijk hier welk product bij je past.", true));
                    }
                    if (C1_X2.Uitslag && C2_4_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je kind minder dan gemiddeld moeite lijkt te hebben met doorslapen.<br/>Als je tóch een product voor je kind wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (C1_X2.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je kind meer dan gemiddeld moeite lijkt te hebben met doorslapen.<br/>Kijk hier welk product bij je past.", true));
                    }
                    if (C1_X3.Uitslag && !C3_1_X3.Uitslag && C3_2_X1.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je kind minder dan gemiddeld moeite lijkt te hebben met te vroeg wakker worden.<br/>Als je tóch een product voor je kind wilt proberen, kijk dan hier voor een laag gedoseerd product.", false));
                    }
                    else if (C1_X3.Uitslag) {
                        advies.push(new AdviesData(AdviesID.R, "De test wijst uit dat je kind meer dan gemiddeld moeite lijkt te hebben met te vroeg wakker worden.<br/>Kijk hier welk product bij je past.", true));
                    }
                    if (B1_X1.Uitslag && (D1_X1.Uitslag || D1_X2.Uitslag)) {
                        if (!D1_4_X1.Uitslag) {
                            advies.push(new AdviesData(AdviesID.O, "De test wijst uit dat je kind wel eens gevoelens kent van spanning of nervositeit en langer druk lijkt te zijn.<br/>Kijk hier welk product bij je past.", true));
                        }
                        else {
                            advies.push(new AdviesData(AdviesID.O, "De test wijst uit dat je kind wel eens gevoelens kent van spanning of nervositeit.<br/>Kijk hier welk product bij je past.", true));
                        }
                    }
                    if (B1_X2.Uitslag && (D1_X1.Uitslag || D1_X2.Uitslag)) {
                        if (!D1_4_X1.Uitslag) {
                            advies.push(new AdviesData(AdviesID.P, "De test wijst uit dat je kind wel eens gevoelens kent van spanning of nervositeit en langer druk lijkt te zijn.<br/>Kijk hier welk product bij je past.", true));
                        }
                        else {
                            advies.push(new AdviesData(AdviesID.P, "De test wijst uit dat je kind wel eens gevoelens kent van spanning of nervositeit.<br/>Kijk hier welk product bij je past.", true));
                        }
                    }
                    if (advies.length == 0) {
                        advies.push(new AdviesData(AdviesID.NONE, "De test wijst uit dat je kind minder dan gemiddeld moeite lijkt te hebben met inslapen, spanning of nervositeit. Heb je toch het gevoel meer te willen weten over deze onderwerpen? Kijk dan eens op www.dagennachtrust.nl of vraag uw drogist.", false));
                    }
                }
            }
            catch (e) {
                alert('Antwoord bestaat niet.');
            }
            return advies;
        }
    }
    exports.Test = Test;
    class AdviesCaroussel extends BaseScreen {
        constructor() {
            super('adviesCarroussel');
            this._producten = null;
            $('#btnCloseAdvies').click(e => this.close(e));
            $('#btnStartOpnieuwTest').click(e => this.close(e));
            $('#btnBackTwo').click(e => this.close(e));
            $("#hider").hide();
            $('#btnPopupClose').click(e => this.closePopup(e));
            $("#hider").click(e => this.closePopup(e));
            var elem = document.getElementById('imageContainer');
            var hammertime = new hammerjs(elem);
            var parent = this;
            hammertime.add(new hammerjs.Pan({ direction: hammerjs.DIRECTION_HORIZONTAL, threshold: 80, pointers: 0 }));
            hammertime.on("panend", function (ev) {
                parent.slideProduct(ev.direction == hammerjs.DIRECTION_RIGHT);
            });
            hammertime.on('tap', function (ev) {
                ev.srcEvent.preventDefault();
                if (ev.pointers[0].clientX < (1024 / 2)) {
                    parent.slideProduct(true);
                }
                else {
                    parent.slideProduct(false);
                }
                ev.srcEvent.stopPropagation();
            });
        }
        close(e) {
            e.preventDefault();
            Screens.getInstance().showStartScreen();
            e.stopPropagation();
        }
        closePopup(e) {
            e.preventDefault();
            $("#hider").hide();
            e.stopPropagation();
        }
        slide(e) {
            e.preventDefault();
            if (e.pageX < (1024 / 2)) {
                this.slideProduct(true);
            }
            else {
                this.slideProduct(false);
            }
            e.stopPropagation();
        }
        init(adviezen) {
            this._producten = new Array();
            console.log(adviezen)
            adviezen.forEach(advies => {
                if (advies.ID == AdviesID.A) {
                    this._producten.push(new ProductCarousselData(ProductID.A1));
                    // this._producten.push(new ProductCarousselData(ProductID.A2));
                    // this._producten.push(new ProductCarousselData(ProductID.A3));
                    // this._producten.push(new ProductCarousselData(ProductID.A4));
                    // this._producten.push(new ProductCarousselData(ProductID.A5));
                    // this._producten.push(new ProductCarousselData(ProductID.A6));
                    // this._producten.push(new ProductCarousselData(ProductID.A7));
                }
                else if (advies.ID == AdviesID.B) {
                    this._producten.push(new ProductCarousselData(ProductID.B1));
                    // this._producten.push(new ProductCarousselData(ProductID.B2));
                    // this._producten.push(new ProductCarousselData(ProductID.B3));
                    // this._producten.push(new ProductCarousselData(ProductID.B4));
                    // this._producten.push(new ProductCarousselData(ProductID.B5));
                }
                else if (advies.ID == AdviesID.C) {
                    this._producten.push(new ProductCarousselData(ProductID.C1));
                }
                else if (advies.ID == AdviesID.D) {
                    this._producten.push(new ProductCarousselData(ProductID.D1));
                    // this._producten.push(new ProductCarousselData(ProductID.D2));
                    // this._producten.push(new ProductCarousselData(ProductID.D3));
                    // this._producten.push(new ProductCarousselData(ProductID.D4));
                }
                else if (advies.ID == AdviesID.I) {
                    this._producten.push(new ProductCarousselData(ProductID.I1));
                }
                else if (advies.ID == AdviesID.K) {
                    this._producten.push(new ProductCarousselData(ProductID.K1));
                    // this._producten.push(new ProductCarousselData(ProductID.K2));
                }
                else if (advies.ID == AdviesID.J) {
                    this._producten.push(new ProductCarousselData(ProductID.J1));
                }
                else if (advies.ID == AdviesID.L) {
                    this._producten.push(new ProductCarousselData(ProductID.L1));
                }
                else if (advies.ID == AdviesID.NONE) {
                    this._producten.push(new ProductCarousselData(ProductID.NONE));
                }
            });
            this.createCaroussel();
        }
        slideProduct(toRight) {
            var changed = false;
            if (toRight && this._current > 0) {
                changed = true;
                this._current--;
            }
            else if (!toRight && this._current < (this._producten.length - 1)) {
                changed = true;
                this._current++;
            }
            if (changed) {
                var product = this._producten[this._current];
                this.fillProductAnimated(product, toRight);
            }
        }
        createCaroussel() {
            this._current = 0;
            var product = this._producten[this._current];
            this.fillProduct(product);
            $("#bullet1").hide();
            $("#bullet2").hide();
            $("#bullet3").hide();
            $("#bullet4").hide();
            $("#bullet5").hide();
            $("#bullet6").hide();
            $("#bullet7").hide();
            $("#bullet8").hide();
            var length = this._producten.length;
            while (length > 0) {
                $("#bullet" + length.toString()).show();
                length--;
            }
        }
        fillProductAnimated(product, toRight) {
            var speed = '400';
            $('#imageLeft').animate({
                'marginLeft': (toRight ? "+" : "-") + "300px",
                'marginRight': (toRight ? "-" : "+") + "300px",
                'opacity': (toRight ? "1" : "0.6"),
                'height': (toRight ? "250px" : "200px")
            }, speed, () => {
                $('#imageLeft').css('marginLeft', "0px");
                $('#imageLeft').css('marginRight', "0px");
                $('#imageLeft').css('opacity', "0.6");
                $('#imageLeft').css('height', "200px");
            });
            $('#imageRight').animate({
                'marginLeft': (toRight ? "+" : "-") + "300px",
                'marginRight': (toRight ? "-" : "+") + "300px",
                'opacity': (toRight ? "0.6" : "1"),
                'height': (toRight ? "200px" : "250px")
            }, speed, () => {
                $('#imageRight').css('marginLeft', "0px");
                $('#imageRight').css('marginRight', "0px");
                $('#imageRight').css('opacity', "0.6");
                $('#imageRight').css('height', "200px");
            });
            $('#header').animate({
                'opacity': "0",
            }, speed, () => {
                $('#header').animate({
                    'opacity': "1",
                }, speed, () => { });
            });
            $('#titel').animate({
                'opacity': "0",
            }, speed, () => {
                $('#titel').animate({
                    'opacity': "1",
                }, speed, () => { });
            });
            $('#onderTitel').animate({
                'opacity': "0",
            }, speed, () => {
                $('#onderTitel').animate({
                    'opacity': "1",
                }, speed, () => { });
            });
            $('#imageCenter').animate({
                'marginLeft': (toRight ? "+" : "-") + "300px",
                'marginRight': (toRight ? "-" : "+") + "300px",
                'opacity': "0.6",
                'height': "200px"
            }, speed, () => {
                $('#imageCenter').css('marginLeft', "0px");
                $('#imageCenter').css('marginRight', "0px");
                $('#imageCenter').css('opacity', "1");
                $('#imageCenter').css('height', "250px");
                this.fillProduct(product);
            });
        }
        fillProduct(product) {
            $("#header").html(product.Header);
            if (product.Image != "") {
                $("#imageCenter").attr({style : ""});
                $("#imageCenter").show();
                $("#imageCenter").attr("src", "Content/Products/" + product.Image);
                // $("#imageCenter").css('transform', "scale(1)");
                // $("#imageCenter").css('margin-top', "0px");
                if(product.Header === "Advies Kaart!"){
                    $("#header").html("");
                    let x = window.matchMedia("(min-width: 1920px)")
                    console.log(x.matches)
                    if(x.matches){
                        $('#imageCenter').css('transform', "scale(3)");
                        $('#imageCenter').css('margin-top', "150px");

                    }else {
                        $('#imageCenter').css('transform', "scale(3)");
                        $('#imageCenter').css('margin-left', "-222px");
                        $('#imageCenter').css('position', "fixed");
                    }
                }
            }
            else {
                $("#imageCenter").hide();
                $("#imageCenter").css('transform', "scale(1)");
                $("#imageCenter").css('margin-top', "0px");
            }
            $("#titel").html("")
            $("#onderTitel").html("")
            $("#titel").html(product.Html);
            $("#onderTitel").html(product.Ondertitel);
            var imageLeft = "";
            if (this._current > 0) {
                imageLeft = "Content/Products/" + this._producten[(this._current - 1)].Image;
            }
            $("#imageLeft").attr("src", imageLeft);
            var imageRight = "";
            if (this._current < (this._producten.length - 1)) {
                imageRight = "Content/Products/" + this._producten[(this._current + 1)].Image;
            }
            $("#imageRight").attr("src", imageRight);
            $("#bullet1").attr("src", "Content/bullet_orange.png");
            $("#bullet2").attr("src", "Content/bullet_orange.png");
            $("#bullet3").attr("src", "Content/bullet_orange.png");
            $("#bullet4").attr("src", "Content/bullet_orange.png");
            $("#bullet5").attr("src", "Content/bullet_orange.png");
            $("#bullet6").attr("src", "Content/bullet_orange.png");
            $("#bullet7").attr("src", "Content/bullet_orange.png");
            $("#bullet8").attr("src", "Content/bullet_orange.png");
            $("#bullet" + (this._current + 1).toString()).attr("src", "Content/bullet_orange_fill.png");
        }
    }
    exports.AdviesCaroussel = AdviesCaroussel;
    class Advies extends BaseScreen {
        constructor() {
            super('advies');
            this._adviezen = null;
            $('#btnCloseAdvies').click(e => this.close(e));
            $('#btnStartOpnieuwTest').click(e => this.close(e));
            $('#btnBackTwo').click(e => this.back(e));
            $("#hider").hide();
            $('#btnPopupClose').click(e => this.closePopup(e));
            $("#hider").click(e => this.closePopup(e));
        }
        back(e) {
            e.preventDefault();
            Screens.getInstance().showTestScreen();
            e.stopPropagation();
        }
        close(e) {
            e.preventDefault();
            Screens.getInstance().showStartScreen();
            e.stopPropagation();
        }
        closePopup(e) {
            e.preventDefault();
            $("#hider").hide();
            e.stopPropagation();
        }
        showProduct(e, advies) {
            e.preventDefault();
            var data = new ProductData(advies.ID);
            $("#popupTitle").html(data.Header);
            $("#popupText").html(data.Html);
            $("#popupImage").attr("src", "Content/Products/" + data.Image);
            $("#hider").show();
            e.stopPropagation();
        }
        init(adviezen) {
            this._adviezen = adviezen;
            $('#adviesNone').hide();
            $('.advies', '#adviezen').remove();
            $('.adviesnotactive', '#adviezen').remove();
            var minder = false;
            this._adviezen.forEach(advies => {
                if (advies.ID == AdviesID.NONE) {
                    $('#adviesNone').show();
                }
                else {
                    var divAdvies = document.createElement("div");
                    divAdvies.setAttribute("class", advies.WhiteButton ? "advies" : "adviesnotactive");
                    var divAdviesText = document.createElement("div");
                    divAdviesText.setAttribute("class", "adviestext");
                    divAdviesText.innerHTML = advies.HTML;
                    divAdvies.appendChild(divAdviesText);
                    $('#adviezen').append(divAdvies);
                    $(divAdvies).click(e => this.showProduct(e, advies));
                }
            });
        }
    }
    exports.Advies = Advies;
    class Product extends BaseScreen {
        constructor() {
            super('product');
        }
    }
    exports.Product = Product;
    class Screens {
        constructor() {
            this.start = new Start();
            this.test = new Test();
            this.advies = new AdviesCaroussel();
            this.product = new Product();
            this.currentScreen = null;
            if (Screens._instance) {
                throw new Error("Error: Instantiation failed: Use Screens.getInstance() instead of new.");
            }
            Screens._instance = this;
        }
        static getInstance() {
            return Screens._instance;
        }
        showStartScreen() {
            this.test.reset();
            this.start.showScreen(this.getScreenName());
            this.currentScreen = this.start;
        }
        showTestScreen() {
            this.test.back();
            this.test.showScreen(this.getScreenName());
            this.currentScreen = this.test;
        }
        showAdviesScreen() {
            this.advies.init(this.test.getAdvies());
            this.advies.showScreen(this.getScreenName());
            this.currentScreen = this.advies;
        }
        getScreenName() {
            if (this.currentScreen != null) {
                return this.currentScreen.getScreenName();
            }
            return "";
        }
        showProductScreen() {
            this.product.showScreen(this.getScreenName());
            this.currentScreen = this.product;
        }
    }
    exports.Screens = Screens;
    Screens._instance = new Screens();
});
